import os
from fabric import Connection, task
   
@task
def deploy(ctx):
    #BACKEND
    with Connection(
        '3.95.160.159',
        user="ubuntu",
        connect_kwargs={"key_filename": "keyserversf2.pem"},
    ) as c:
        with c.cd("/home/ubuntu"):
            c.run('docker-compose -f "docker-compose-production.yml" down || echo Compose_Fial')
            c.run('docker rmi carlostorres10/archivo')
            c.run('docker rmi carlostorres10/microservicecarpeta')
            c.run('docker rmi carlostorres10/microserviceusuario')
            c.run('docker rmi carlostorres10/microservicebacklog')
            c.run('docker rmi carlostorres10/usuario')
            c.run('docker rmi carlostorres10/middleware')
            c.put('docker-compose-production.yml')
            c.run('docker-compose -f "docker-compose-production.yml" up -d')
    
    #FRONTEND1
    with Connection(
        '54.159.128.248',
        user="ubuntu",
        connect_kwargs={"key_filename": "keyserversf2.pem"},
    ) as c:
        with c.cd("/home/ubuntu"):
            c.run('docker-compose -f "docker-compose-productionfront.yml" down || echo Compose_Fial')
            c.run('docker rmi carlostorres10/notusangular')
            c.put('docker-compose-productionfront.yml')
            c.run('docker-compose -f "docker-compose-productionfront.yml" up -d')
    
    #FRONTEND2
    with Connection(
        '54.234.36.130',
        user="ubuntu",
        connect_kwargs={"key_filename": "keyserversf2.pem"},
    ) as c:
        with c.cd("/home/ubuntu"):
            c.run('docker-compose -f "docker-compose-productionfront.yml" down || echo Compose_Fial')
            c.run('docker rmi carlostorres10/notusangular')
            c.put('docker-compose-productionfront.yml')
            c.run('docker-compose -f "docker-compose-productionfront.yml" up -d')  