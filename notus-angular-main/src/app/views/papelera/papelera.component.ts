import { Component, OnInit } from '@angular/core';
import { browser } from 'protractor';
import { UsuarioService } from '../../services/usuario.service'
import { ArchivoService } from '../../services/archivo.service'
import { PeticionesService } from '../../services/peticiones.service';
import { Router, RouterLink } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-papelera',
  templateUrl: './papelera.component.html'
})
export class PapeleraComponent implements OnInit {

  filesFolder = [];
  carpetaActual = null;
  archivoSeleccionado = null;


  constructor(private archivoService: ArchivoService,private router: Router, private ServicePeticiones: PeticionesService,  private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.getFolderContent();
    
  }

  async getFolderContent(){
    await this.usuarioService.getSelectedFolder().toPromise().then(
      (res: any) => {
        this.carpetaActual = res.carpetas;

        let tempDate: Date = null;
        (this.carpetaActual.archivos as []).forEach((archivo: any) => {
          tempDate = new Date(archivo.fecha_subida);
          archivo.fecha_subida = `${tempDate.getDate()} / ${tempDate.getMonth()} / ${tempDate.getFullYear()}`;
          archivo.iconColor = "bg-red-500";

          this.filesFolder.push(archivo);
        });
      },
      err => console.error(err)
    )
  }

  selectFile(archivo: any){

    const tempFolder = this.archivoService.getSelectedFolderUser();
    this.archivoService.setSelectedFile(tempFolder.id_user,tempFolder.id_carpeta,archivo.id_archivo);

    if(this.archivoSeleccionado && archivo.id_archivo != this.archivoSeleccionado.id_archivo){
      this.archivoSeleccionado.iconColor = "bg-red-500";
    }

    archivo.iconColor = archivo.iconColor == "bg-red-500" ? "bg-indigo-500" : "bg-red-500";
    this.archivoSeleccionado = archivo;
  }

  getIntoFile(file){
     if(file.extension == "jpg" || file.extension == "jpeg" || file.extension == "png" || file.extension == "gif" || file.extension == "svg" ||
        file.extension == "mp4" || file.extension == "avi" || file.extension == "flv" || file.extension == "mov" || file.extension == "txt"){
      window.open(file.link,"_blank");
     }
     else{
      this.archivoService.downloadFile(file.link).subscribe(archivo => {

        //Creamos un archivo blob con nuestro archivo en S3
        var newBlob = new Blob([archivo], { type: `application/${file.extension}`});
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob, file.nombre_archivo);
          return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = file.nombre_archivo;
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
  
       });
     }
  }

  async deleteFile() {
    const file = JSON.parse(localStorage.getItem("currentFile"));
    file.ruta = "rmfile";

    await this.archivoService.deleteFile(file).toPromise().then(
      res => {
        console.log(res);
        localStorage.setItem('currentFile', "");
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: "El archivo ha sido eliminado definitivamente con exito",
          showConfirmButton: false,
          timer: 2000
        })
        this.ServicePeticiones.actualizarPagina();
        //window.location.reload();
      },
      err => {
        console.error(err);
      }
    );
  }

  async restaurar() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const file = JSON.parse(localStorage.getItem("currentFile"));

    var datos = {
      ruta: "movfile",
      id_user: user.id_user,
      id_carpeta: file.id_carpeta,
      id_archivo: file.id_archivo,
      id_carpeta_nueva: localStorage.getItem("oldCarpeta")
    }
    console.log(datos)

    await this.archivoService.moveFile(datos).toPromise().then(
      res => {
        console.log(res);
        localStorage.setItem('currentFile', "");
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: "El archivo ha sido restaurado con exito",
          showConfirmButton: false,
          timer: 2000
        })
        this.ServicePeticiones.actualizarPagina();
        //window.location.reload();
      },
      err => {
        console.error(err);
      }
    );
  }

}