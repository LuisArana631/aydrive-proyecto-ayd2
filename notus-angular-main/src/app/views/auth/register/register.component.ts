import { Component, OnInit } from "@angular/core";
import { UsuarioService } from '../../../services/usuario.service'
import { Router } from '@angular/router';

import Swal from 'sweetalert2'


@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
})
export class RegisterComponent implements OnInit {

    public newUser = {
      usuario: "",
      nombre: "",
      fecha_nacimiento: "",
      correo: "",
      numero: "",
      contrasena: "",
      contrasena2: ""
    }
    public politicas = false;
    public showAlert = false;
    public messageAlert = '';

  constructor(private usuarioService: UsuarioService, private router: Router) {}

  ngOnInit(): void {}

  //Recibe los datos para la creación de un nuevo usuario
  async createUser(user) {
    if(this.politicas){

      await this.usuarioService.createUser(
        user
      ).subscribe(
        (res: any) => {
          if ((!res.message.includes('Error'))){
            Swal.fire({
              title: 'Usuario Creado correctamente',
              width: 600,
              padding: '3em',
              background: '#fff url(/assets/img/trees.png)',
              backdrop: `
                rgba(0,0,123,0.4)
                url("/assets/img//confetti.gif")
                center top
                no-repeat
              `
            }) 
            this.router.navigate(["/"]);
          }
          else{
            this.showAlert = true;
            this.messageAlert = (res.message as string).split('Error:')[1]
          }
        },
        err => console.error(err)
      )
    }
    else{
      this.showAlert = true;
      this.messageAlert = "Debe aceptar las politicas de privacidad"
    }
  }
}
