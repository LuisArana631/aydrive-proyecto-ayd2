import { Component, OnInit } from '@angular/core';
import { ArchivoService } from '../../services/archivo.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-editar-archivo',
  templateUrl: './editar-archivo.component.html'
})
export class EditarArchivoComponent implements OnInit {

  nombreArchivo = ""

  archivoActual: any = {
    extension: "",
    fecha_subida: "",
    id_archivo: "",
    link: "",
    nombre_archivo: ""
  };

  constructor(private archivoService: ArchivoService, private location: Location) { }

  ngOnInit(): void {
    this.obtenerInfo()
  }

  async obtenerInfo(){
    await this.archivoService.getSelectedFile().toPromise().then(
      (res: any) => {
        this.archivoActual = res.archivo;
        this.nombreArchivo = this.archivoActual.nombre_archivo;
      },
      err => {
        console.error(err);
      }
    )
  }

  async editFile(){
    const editFile = {
      ruta: "mvfile",
      id_user: this.archivoService.getSelectedFolderUser().id_user,
      id_carpeta: this.archivoService.getSelectedFolderUser().id_carpeta,
      id_archivo: this.archivoActual.id_archivo,
      nombre: this.archivoActual.nombre_archivo
    }

    await this.archivoService.editFile(editFile).toPromise().then(
      res => {
        this.location.back();
      },
      err => {
        console.error(err);
      }
    );
  }

}
