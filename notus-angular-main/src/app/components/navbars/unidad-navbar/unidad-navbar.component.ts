import { Component, OnInit } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

// IMPORTACIÓN DEL SERVICIO PETICIONES
import{PeticionesService} from '../../../services/peticiones.service';

@Component({
  selector: 'app-unidad-navbar',
  templateUrl: './unidad-navbar.component.html',
})
export class UnidadNavbarComponent implements OnInit {
  navbarOpen = false;
  UserActual="";

  constructor(public ServicePeticiones: PeticionesService,private router: Router) { }

  ngOnInit(): void {
    this.UserActual = this.ServicePeticiones.ObtenerUser();
  }
  setNavbarOpen() {
    this.navbarOpen = !this.navbarOpen;
  }
  CerrarSesion(){
    this.ServicePeticiones.CerrarSesion();
  }
}
