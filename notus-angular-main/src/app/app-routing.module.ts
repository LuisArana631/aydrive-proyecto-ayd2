import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// layouts
import { AdminComponent } from "./layouts/admin/admin.component";
import { AuthComponent } from "./layouts/auth/auth.component";
import { UserComponent } from './layouts/user/user.component';
import { DeniedComponent } from './layouts/denied/denied.component';

// admin views
import { DashboardComponent } from "./views/admin/dashboard/dashboard.component";
import { MapsComponent } from "./views/admin/maps/maps.component";
import { SettingsComponent } from "./views/admin/settings/settings.component";
import { TablesComponent } from "./views/admin/tables/tables.component";

// auth views
import { LoginComponent } from "./views/auth/login/login.component";
import { RegisterComponent } from "./views/auth/register/register.component";
import { RestringidoComponent } from './views/auth/restringido/restringido.component';


// no layouts views
import { IndexComponent } from "./views/index/index.component";
import { LandingComponent } from "./views/landing/landing.component";
import { ProfileComponent } from "./views/profile/profile.component";
import { MiunidadComponent } from './views/miunidad/miunidad.component';
import { CarpetaComponent } from './views/carpeta/carpeta.component';
import { SubirArchivoComponent } from './views/subir-archivo/subir-archivo.component';
import { EditarArchivoComponent } from './views/editar-archivo/editar-archivo.component';
import { PapeleraComponent } from './views/papelera/papelera.component';

const routes: Routes = [
  // admin views
  {
    path: "admin",
    component: AdminComponent,
    children: [
      { path: "dashboard", component: DashboardComponent },
      { path: "settings", component: SettingsComponent },
      { path: "tables", component: TablesComponent },
      { path: "maps", component: MapsComponent },
      { path: "", redirectTo: "dashboard", pathMatch: "full" },
    ],
  },
  // auth views
  {
    path: "auth",
    component: AuthComponent,
    children: [
      { path: "login", component: LoginComponent },
      { path: "register", component: RegisterComponent },
      { path: "", redirectTo: "login", pathMatch: "full" },
    ],
  },

  // unidad restringido
  {
    path: "auth",
    component: DeniedComponent,
    children: [
      { path: "restringido", component: RestringidoComponent },
      { path: "", redirectTo: "restringido", pathMatch: "full" },
    ],
  },

  // unidad views
  {
    path: "user",
    component: UserComponent,
    children: [
      { path: "miunidad", component: MiunidadComponent },
      { path: "", redirectTo: "miunidad", pathMatch: "full"},
      { path:"carpeta", component: CarpetaComponent },
      { path:"subir-archivo", component: SubirArchivoComponent },
      { path: "papelera", component: PapeleraComponent },
      { path:"editar-archivo", component: EditarArchivoComponent }
    ],
  },

  // no layout views
  { path: "profile", component: ProfileComponent },
  { path: "landing", component: LandingComponent },
  { path: "", component: IndexComponent },
  { path: "**", redirectTo: "", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
