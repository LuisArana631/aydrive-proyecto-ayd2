const { Response, Request } = require('express');
const aws_keys = require('../models/creds');
const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient(aws_keys.dynamodb);
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const bcryptjs = require('bcryptjs');
const sns = new AWS.SNS(aws_keys.sns);
var nodemailer = require('nodemailer'); 
const Speakeasy = require('speakeasy');

const registrarUsuario = (req = Request, res = Response) => {
    let body = req.body;
    let usuario= body.usuario;
    let nombre=body.nombre;
    let fecha_nacimiento=body.fecha_nacimiento;
    let correo =  body.correo;
    let contrasena = body.contrasena;
    let contrasena2 = body.contrasena2;
    let numero = body.numero;
    let id_user = crypto.createHash('sha1').update(usuario + contrasena).digest('hex');
    let secret = Speakeasy.generateSecret({ length: 20 });

    let contraIgual = true;
    let existe_user = false;
    let existe_correo= false;
    const esCorreoElectronico = correo=> /\S+@\S+/.test(correo);

    const esUsuario= usuario=> /^[a-zA-Z][a-zA-Z0-9-_]+$/.test(usuario);

    //var regexp = /^[a-zA-Z0-9\-\_]$/;
    var check = usuario;
    //Comprobar si ya existe Usuario y Correo
    //--Validacion de Usuario--
    let paramsScan = {
        TableName: "aydrive",
        Limit: 100 
    };
    ddb.scan(paramsScan, function(err, data){
        if(err){
            console.log("Error", err);
        }  else{
              for (const usuarios of data.Items) {
                if(usuarios.usuario==usuario){
                    existe_user=true;
                    break;
                }
                if(usuarios.correo==correo){
                    existe_correo=true;
                    break;
                } 
              }
              if(existe_user){
                res.send({
                    'message': 'Error: Nombre de usuario ya existe',
                    'estado':400
                });
              }else if(existe_correo){
                res.send({
                    'message': 'Error: Correo ya en uso',
                    'estado':401
                });
              }else{
                  if(esCorreoElectronico(correo)){
                      if(esUsuario(usuario)){
                        let reciclaje = {
                            "id_user": id_user,
                            "fecha_creacion": new Date().toString(),
                            "nombre": "papelera",
                            "id": uuidv4(),
                            "carpetas": [],
                            "archivos": []
                        }
                        var carpeta ={
                            "id_user":id_user,
                            "fecha_creacion": new Date().toString(),
                            "nombre":"root",
                            "id":uuidv4(),
                            "carpetas":[reciclaje],
                            "archivos":[]
                        }
                        let contraHash = bcryptjs.hashSync(contrasena, 8);             
                        var input = {
                            "id":id_user,
                            "usuario":usuario,
                            "nombre":nombre,
                            "correo":correo,
                            "contrasena":contraHash,
                            "Fecha":fecha_nacimiento,
                            "carpetas":[carpeta],
                            "id_papelera": reciclaje.id,
                            "auth": 0,
                            "numero": numero,
                            "tipo_notificacion": 0,
                            "secret": secret.base32
                        }
                                
                        var params = {
                            TableName: "aydrive",
                            Item:  input
                        };
                          
                        // validar contraseña
                        (contrasena==contrasena2) ? contraIgual = true:contraIgual = false
                        if(contraIgual){
                            ddb.put(params, function (err, data) {     
                                if (err) {
                                    console.log("users::save::error - " + JSON.stringify(err, null, 2));
                                    res.send({
                                        'message': 'Error',
                                    });                   
                                } else {
                                    console.log("users::save::success" );     
                                    res.send({
                                        'message': 'Usuario Registrado con Exito',
                                        'id_user': id_user
                                    });                 
                                }
                            });
                        }else{
                            res.send({
                                'message': 'Error: Ingrese de nuevo la contraseña',
                            });  
                        }
                    }else{
                        res.send({
                            'message': 'Error: Ingrese un usuario valido',
                        }); 
                    }
                    
                  }else{
                    res.send({
                        'message': 'Error: Ingrese un correo valido',
                    }); 
                  }
                
                  
              }
  
  
        }
    });
   
    
}

const loginUsuario = (req = Request, res = Response) => {
    let body = req.body;
    let usuario= body.usuario
    let contrasena = body.contrasena
    //let id_user = crypto.createHash('sha1').update(usuario + contrasena).digest('hex');
    let existe_user =true
    let paramsScan = {
        TableName: "aydrive",
        Limit: 100 
    };
    ddb.scan(paramsScan, function(err, data){
        if(err){
            console.log("Error", err);
        }  else{
              
            for (const usuarios of data.Items) {
                let compare = bcryptjs.compareSync(contrasena, usuarios.contrasena.toString())
                if(usuarios.usuario==usuario && compare){
                    existe_user=true;                    
                    console.log(usuarios);
                    res.send(usuarios);
                    break;
                }else{
                    existe_user=false;
                } 
            }
            if(!existe_user){
                res.send({
                    'message': 'Error: Usuario no encontrado',
                    'estado':400
                });
                //res.send("Error: Usuario no encontrado")
            }
        }
    });
}

const validateToken = (req = Requ, res = Respo) => {
    let body = req.body;
    let secret = body.secret;
    let token = body.token;

    let valido = Speakeasy.totp.verify({
        secret: secret,
        encoding: "base32",
        token: token,
        window : 6 //3 minutos (6 * 30 seg = 3 min)
    }); 

    res.send({
        'message': 'Token Validado',
        'token_valido': valido
    })
}

const editarUsuario = (req = Request, res = Response)  => {
    let body = req.body;
    let id_user= body.id_user;
    let nombre=body.nombre;
    let correo=body.correo;
    let fecha_nacimiento=body.fecha_nacimiento;
    let contrasena=body.contrasena;
    let numero=body.numero;
    let auth_val= body.auth_val;
    let existe_user = false;
    let tipo_notificacion=body.tipo_notificacion;

    ddb.get({
        TableName: "aydrive",
        Key:{
            id: id_user
        }
    }, function(err, data) {
        if(err) {
            console.log('Error al encontrar el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{
               existe_user=true;
            }

            if(existe_user){
                ddb.update({
                    TableName: "aydrive",
                    Key: {
                        id: id_user
                    },
                    UpdateExpression: "SET #nombre=:nombre, #correo=:correo, #Fecha=:Fecha, #auth=:auth, #tipo_notificacion=:tipo_notificacion, #numero=:numero" ,
                    ExpressionAttributeNames: { 
                        "#nombre": "nombre",
                        "#correo": "correo",
                        "#Fecha": "Fecha",
                        "#auth": "auth",
                        "#tipo_notificacion":"tipo_notificacion",
                        "#numero":"numero",
                    },
                    ExpressionAttributeValues: {
                        ":nombre": nombre,
                        ":correo": correo,
                        ":Fecha": fecha_nacimiento,
                        ":auth": auth_val,
                        ":tipo_notificacion":tipo_notificacion,
                        ":numero":numero,
                    }
                }, function(err, data) {
                    if (err) {
                        console.log('Error reading user data:', err);
                        res.send({
                            'message': 'Error al actualizar la informacion del usuario',
                            'estado': 401
                        });
                    } else {
                        res.send({
                            'message': 'Usuario modificado',
                            'estado': 200
                        });
                    }
                });
                  console.log(data.Item);
            }
        }
    })
}

const eliminarUsuario = (req = Request, res = Response)  => {
    let body = req.body;
    let id_user= body.id_user;
    let existe_user = false;

    ddb.get({
        TableName: "aydrive",
        Key:{
            id: id_user
        }
    }, function(err, data) {
        if(err) {
            console.log('Error al encontrar el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{
               existe_user=true;
            }

            if(existe_user){
                const eliminar= {
                    TableName: "aydrive",
                    Key: {
                        id: id_user
                    }
                };
                ddb.delete(eliminar, function (err, data) {
                    if (err) {
                        console.log("Error al eliminar el usuario "+err)
                        res.send({
                            'message': 'Error al eliminar el usuario '+err,
                            'estado': 401
                        });
                    } else{
                        res.send({
                            'message': 'Usuario Eliminado ',
                            'estado': 200,
                        });
                    }
                });
                console.log(data.Item);
            }
        }
    })
}

/**Envio de Emails Y SMS para autenticacion de cuentas */
const autenticar = (req =  Request, res = Response) =>{
    let body = req.body;
    let id_user= body.id_user;
    let secret = body.secret;
    let tipo=body.tipo_notificacion;

    let datos=[];

    let token = Speakeasy.totp({
        secret: secret,
        encoding: "base32",
        window : 6 //3 minutos (6 * 30 seg = 3 min)
    });

    ddb.get({
        TableName: "aydrive",
        Key:{
            id: id_user
        }
    }, async function(err, data) {
        if(err) {
            console.log('Error al encontrar el usuario:', err);
            res.send({
                'message': 'Error al obtener al usuario de la base de datos',
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'Error, el usuario no existe',
                    'estado': 403
                });
            }else{
               datos = data.Item;
               console.log(datos);
               if(tipo == 0){//mando correo
                    let resultado=enviarEmail(datos.usuario, datos.correo, token);
                    if(resultado == 500){
                        res.send({
                            'message': "Error al intentar enviar el correo",
                            'estado': 500
                        });
                    }else{
                        res.send({
                            'message': 'Correo enviado!',
                            'token':token,
                            'estado': 200
                        });
                    }
               }else if(tipo == 1){//mando sms
                
                    let resultado=enviarSMS(datos.usuario,datos.numero,token);
                    if(resultado == 402){
                        res.send({
                            'message': "Error al intentar enviar el SMS",
                            'estado': 402
                        });
                    }else{
                        res.send({
                            'message': "SMS Enviado!",
                            'token':token,
                            'estado': 100
                        });
                    }
               }else if(tipo ==2){//mando ambos
                    let resultado=enviarEmail(datos.usuario, datos.correo, token);
                    if(resultado == 500){
                        res.send({
                            'message': "Error al intentar enviar el correo",
                            'estado': 500
                        });
                    }else{
                        /*res.send({
                            'message': 'Correo enviado!',
                            'estado': 200
                        });*/
                        let resultado2= await enviarSMS(datos.usuario,datos.numero,token);
                        if(resultado2 == 402){
                            res.send({
                                'message': "Error al intentar enviar el SMS",
                                'estado': 402
                            });
                        }else{
                            res.send({
                                'message': "Correo y SMS Enviados!",
                                'token':token,
                                'estado': 200
                            });
                        }
                    }

                    
               }
               
            }
        }
    })
}

function enviarEmail(usuario,correo, token){
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'dcmc2297@gmail.com',
            pass: 'IL0veC4ts.22'
        }
    });

    /*let cuerpo_correo = '<h1>AyDrive Team says:</h1><br><h4>Important information from your account, <br>'+
    '<b>'+texto+'</b></h4> <br> <br> <h5>For more information please contact us: <a href="#">AyDrive Team</a></h5>';*/
    let cuerpo_correo="<!DOCTYPE html>"+ 
    "<body style=\"margin:0;padding:0;word-spacing:normal;background-color:#939297;\">"+
    "<div role=\"article\" aria-roledescription=\"email\"  style=\"text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#939297;\">"+
    "<table role=\"presentation\" style=\"width:100%;border:none;border-spacing:0;\"><tr><td align=\"center\" style=\"padding:0;\">"+
    "<table role=\"presentation\" style=\"width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;\">"+
    "<tr><td style=\"padding:30px;background-color:#ffffff;\"><h1 style=\"margin-top:0;margin-bottom:16px;font-size:26px;line-height:32px;font-weight:bold;letter-spacing:-0.02em;\">AyDrive Team!</h1>"+
    "<p style=\"margin:0;\"><b>"+usuario+"</b> <br><br>Hay informacion importante relacionada a tu cuenta que necesita tu atencion: </p></td></tr><tr>"+
    "<td style=\"padding:35px 30px 11px 30px;font-size:0;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);\">"+
    "<img src=\"https://assets.codepen.io/210284/icon.png\" width=\"115\" style=\"width:80%;max-width:115px;margin-bottom:20px;\">"+
    "<div class=\"col-lge\" style=\"display:inline-block;width:100%;max-width:395px;vertical-align:top;padding-bottom:20px;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;\">"+
    "<p style=\"margin-top:0;margin-bottom:12px;\">Hi! your token is: "+token+"</p><br><br><br><br><br>"+
    "<p style=\"margin-top:0;margin-bottom:12px;\">For more information please:<a href=\"#\">contact us!</a> </p></div></div>"+
    "</td></tr></table></td></tr></table></div></body></html>";

    var mailOptions = {
        from: 'AyDrive Team',
        to: correo,
        subject: "Generacion de Token",
        html: cuerpo_correo
    };

        // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info){
        if (error){
            console.log(error);
            return 500;
        } else {
            console.log("Email sent");
            //res.status(200).jsonp(req.body);
            return 200;
        }
    });
}

async function enviarSMS(usuario, numero, token){

    let mensaje="Welcome to AyDrive "+ usuario +" !, your token is: "+token;
    var params = {
        Message: mensaje,
        PhoneNumber: '+502'+ numero,
        MessageAttributes: {
            'AWS.SNS.SMS.SenderID': {
                'DataType': 'String',
                'StringValue': "AYDRIVE"
            }
        }
    };
    
    var publishTextPromise = sns.publish(params).promise();
    publishTextPromise.then(
        function (data) {
            console.log("Mensaje Enviado! ID:"+data.MessageId)
            return 100;
            //res.end(JSON.stringify({ MessageID: data.MessageId }));
        }).catch(function (err) {
            console.log(err);
            return 402;
                //res.end(JSON.stringify({ Error: err }));
        });
}


module.exports = {
    editarUsuario,
    eliminarUsuario,
    autenticar,
    registrarUsuario,
    loginUsuario,
    validateToken
}