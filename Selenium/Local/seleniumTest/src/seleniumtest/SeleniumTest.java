/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seleniumtest;

/**
 *
 * @author Torres
 */
public class SeleniumTest {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        System.out.println("------- EJECUTANDO LAS PRUEBAS DE AUTOMATIZACIÓN -------");
        Tests Automatizacion = new Tests();
        Automatizacion.RegistroUsuario();
        Automatizacion.AutenticacionFallida();
        Automatizacion.AutenticacionExitosa();
        Automatizacion.CrearCarpeta();
        Automatizacion.EliminarCarpeta();
    }
}
