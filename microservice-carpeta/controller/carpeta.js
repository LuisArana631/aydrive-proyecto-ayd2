const { Response, Request } = require('express');
const aws_keys = require('../models/creds_template');
const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient(aws_keys.dynamodb);
const { v4: uuidv4 } = require('uuid');

const crearCarpeta = (req = Request, res = Response) => {
    let body = req.body;

    // PARAMETROS PARA CREAR UNA CARPETA
    let fecha_creacion = body.fecha_creacion;
    let id_user = body.id_user;
    let nombre_carpeta = body.nombre;
    let id_padre = body.id_padre;

    let existe_user = true;

    // VALIDAR SI EXISTE UN ALBUM
    ddb.get({
        TableName: "aydrive",
        Key:{
            id: id_user
        }
    }, function(err, data) {
        if(err) {
            console.log('Error reading user data:', err);
            res.send({
                'message': 'ddb failed',
                'estado': 400
            });
        } else {
            ( data.Count == 0 ) ? existe_user = false: existe_user = true;

            if ( existe_user ) {
                usuario_item =  data.Item;

                estado = findPathById(usuario_item, id_padre, "insertar", {
                    "id": uuidv4(),
                    "nombre": nombre_carpeta,
                    "fecha_creacion": fecha_creacion,
                    "carpetas": [],
                    "archivos": []
                });

                console.log(estado);

                if ( estado == 200 ){
                    // ACTUALIZAR EL USUARIO    

                    ddb.update({
                        TableName: "aydrive",
                        Key: {
                            id: id_user
                        },
                        UpdateExpression: "SET #carpetas = :carpetas",
                        ExpressionAttributeNames: { "#carpetas": "carpetas" },
                        ExpressionAttributeValues: {
                            ":carpetas": usuario_item.carpetas
                        }
                    }, function(err, data){
                        if (err) {
                            console.log('Error creating folder: ', err);
                            res.send({
                                'message': 'dbd failed',
                                'estado': 400
                            });
                        } else {
                            console.log('Save folder success:', data);
                            res.send({
                                'message': 'ddb success',
                                'estado': 200
                            });
                        }
                    })

                } else if( estado == 402 ){
                    // NOTIFICAR DUPLICADO
                    console.log('Error creating folder: duplicado');
                    res.send({
                        'message': 'dbd folder falied - duplicated folder',
                        'estado': estado
                    });    
                } else {
                    // NOTIFICAR ERROR
                    console.log('Error creating folder: ', err);
                    res.send({
                        'message': 'dbd folder falied - path not found',
                        'estado': estado
                    });    
                }

            } else {
                console.log('Error creating folder: ', err);
                res.send({
                    'message': 'dbd folder falied - user not found',
                    'estado': 403
                });
            }
        }
    })
}

const borrarCarpeta = (req = Request, res = Response) => {
    let body = req.body;

    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, function(err, data) {
        if (err) {
            console.log('Error deleting folder data:', err);
            res.send({
                'message': 'ddb failed',
                'estado': 400
            });
        } else {
            if ( data.Count != 0 ){
                usuario_item = data.Item;

                estado = findPathById(usuario_item, id_carpeta, "borrar", {});
                
                if( estado == 200 ){
                    ddb.update({
                        TableName: "aydrive",
                        Key: {
                            id: id_user
                        },
                        UpdateExpression: "SET #carpetas = :carpetas",
                        ExpressionAttributeNames: { "#carpetas": "carpetas" },
                        ExpressionAttributeValues: {
                            ":carpetas": usuario_item.carpetas
                        }
                    }, function(err, data) {
                        if (err) {
                            console.log('Error reading user data:', err);
                            res.send({
                                'message': 'ddb failed',
                                'estado': 400
                            });
                        } else {
                            res.send({
                                'message': 'ddb success',
                                'estado': 200
                            });
                        }
                    });
                }else{
                    console.log('Error reading user data:', err);
                    res.send({
                        'message': 'ddb failed - folder not found',
                        'estado': 404
                    });
                }
            } else {
                console.log('Error deleting folder data:', err);
                res.send({
                    'message': 'ddb failed - user not found',
                    'estado': 403
                });
            }
        }
    });
}

const actualizarCarpeta = (req = Request, res = Response) => {
    let body = req.body;

    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;
    let nombre = body.nombre;

    let existe_user = true;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, function(err, data){
        if (err) {
            console.log('Error reading user data:', err);
            res.send({
                'message': 'ddb failed',
                'carpeta': {},
                'estado': 400
            });
        } else {

            ( data.Count == 0 ) ? existe_user = false: existe_user = true;

            if ( existe_user ) {

                user_item = data.Item;

                estado = findPathById(user_item, id_carpeta, "editar", {
                    "nombre": nombre
                });

                if( estado == 200 ){

                    ddb.update({
                        TableName: "aydrive",
                        Key: {
                            id: id_user
                        },
                        UpdateExpression: "SET #carpetas = :carpetas",
                        ExpressionAttributeNames: { "#carpetas": "carpetas" },
                        ExpressionAttributeValues: {
                            ":carpetas": user_item.carpetas
                        }
                    }, function(err, data){
                        if (err) {
                            console.log('Error creating folder: ', err);
                            res.send({
                                'message': 'dbd failed',
                                'estado': 400
                            });
                        } else {
                            console.log('Save folder success:', data);
                            res.send({
                                'message': 'ddb success',
                                'estado': 200
                            });
                        }
                    });

                } else if( estado == 402 ) {

                    // NOTIFICAR DUPLICADO
                    console.log('Error creating folder: ', err);
                    res.send({
                        'message': 'dbd folder falied - duplicated folder',
                        'estado': estado
                    });    

                } else {

                    // NOTIFICAR ERROR
                    console.log('Error creating folder: ', err);
                    res.send({
                        'message': 'dbd folder falied - path not found',
                        'estado': estado
                    });    

                }

            } else {
                console.log('Error updating folder: ', err);
                res.send({
                    'message': 'dbd folder falied - user not found',
                    'carpeta': {},
                    'estado': 403
                });
            }

        }
    })
}

const verCarpeta = (req = Request, res = Response) => {
    let body = req.body;

    let id_user = body.id_user;
    let id_carpeta = body.id_carpeta;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, function(err, data){
        if (err) {
            console.log('Error reading user data:', err);
            res.send({
                'message': 'ddb failed',
                'carpetas': [],
                'estado': 400
            });
        } else {
            if (data.Count == 0){
                res.send({
                    'message': 'ddb success - user not found',
                    'carpetas': [],
                    'estado': 403
                });
            }else{ 
                user_item = data.Item;

                carpeta = findPathById(user_item, id_carpeta, "ver", {});

                if( carpeta == 400 ){

                    res.send({
                        'message': 'ddb success - folder not found',
                        'carpetas': [],
                        'estado': 404
                    });

                }  else if ( carpeta == 404) {
                    res.send({
                        'message': 'ddb success - path not found',
                        'carpetas': [],
                        'estado': 200
                    });
                } else {

                    res.send({
                        'message': 'ddb success',
                        'carpetas': carpeta,
                        'estado': 200
                    });
                    
                }
            }            
        }
    });
}

const rootCarpeta = (req = Request, res = Response) => {
    let body = req.body;

    let id_user = body.id_user;

    ddb.get({
        TableName: "aydrive",
        Key: {
            id: id_user
        }
    }, function(err, data){
        if (err) {
            console.log('Error reading user data:', err);
            res.send({
                'message': 'ddb failed',
                'carpetas': [],
                'estado': 400
            });
        } else {
            console.log('Read success:', data);

            if (data.Count == 0){
                res.send({
                    'message': 'ddb success - user not found',
                    'carpetas': [],
                    'estado': 403
                });
            }else{ 
                res.send({
                    'message': 'ddb success',
                    'carpetas': data.Item.carpetas,
                    'estado': 200
                });
            }            
        }
    });
}

function findPathById(arreglo, id, accion, carpeta_nueva){
    if(arreglo.carpetas) {
        for (let i in arreglo.carpetas)  {
            if (arreglo.carpetas[i].id == id){
                if(accion == "insertar"){                    
                    // VALIDAR QUE NO TENGA CARPETA CON EL MISMO NOMBRE
                    if(arreglo.carpetas[i].carpetas.find(carp => carp.nombre === carpeta_nueva.nombre) == undefined){
                        arreglo.carpetas[i].carpetas.push(carpeta_nueva);
                        return 200;
                    } else {
                        return 402;
                    }
                }else if(accion == "editar"){
                    if(arreglo.carpetas[i].carpetas.find(carp => carp.nombre === carpeta_nueva.nombre) == undefined){
                        arreglo.carpetas[i].nombre = carpeta_nueva.nombre;
                        return 200;
                    } else {
                        return 402;
                    }
                }else if(accion == "ver"){
                    return arreglo.carpetas[i];
                }else{
                    delete arreglo.carpetas[i];
                    return 200;
                }
            }else if(arreglo.carpetas[i].carpetas.length){
                return findPathById(arreglo.carpetas[i], id, accion, carpeta_nueva);
            }
        }
    }

    return 404;
}


module.exports = {
    crearCarpeta,
    borrarCarpeta,
    actualizarCarpeta,
    verCarpeta,
    rootCarpeta
}